
export interface CardType {
    image: string,
    background_color: string,
    title: string,
    description: string,
    text_color: string
}