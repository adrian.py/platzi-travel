import Home from '../components/Home';
import ListadoCards from 'components/recomendados/Listado';
import Head from 'next/head';
import ListadoTop from '../components/destacadas/Listado';
import Faqs from 'components/Faqs';
import Footer from 'components/layouts/Footer';
import TabBar from 'components/layouts/TabBar';
import Navbar from 'components/layouts/Navbar';

const Index = () => {
  return (
    <div>
      <Head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      </Head>
      <main className="font-Montserrat dark:bg-gray-900">
        <nav className="w-full h-14 hidden bg-white lg:flex p-4 justify-between fixed z-10 dark:bg-gray-800">
          <Navbar />
        </nav>
        <section className="w-full h-screen">
          <div id="home" className="w-full h-3/4">
            <Home />
          </div>
          <div className="w-full h-auto dark:bg-gray-900" id="recomendadas">
            <p className="text-3xl text-primary font-semibold dark:text-white pl-6 pt-6">Recomendados</p>
            <div className="w-auto h-72 items-center overflow-x-auto overscroll-x-contain flex space-x-6 overflow-y-hidden">
              <ListadoCards />
            </div>
          </div>
          <div id="rentas_destacadas" className="px-2 h-full w-full dark:bg-gray-900">
            <p className="text-3xl text-primary font-semibold pb-6 mt-6 dark:text-white pl-6 pt-6">Rentas destacadas</p>
            <ListadoTop />
          </div>
          <div id="faqs" className="w-full h-auto mt-10 dark:bg-gray-900">
            <Faqs />
          </div>

          <Footer />
        </section>

        <div id="tab_bar" className="w-full h-16 bg-white fixed left-0 bottom-0 shadow-md flex space-x-8 items-center justify-center lg:hidden dark:bg-gray-800">
          <TabBar />
        </div>

      </main>
    </div>
  )
}
export default Index;
