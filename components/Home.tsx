import React from 'react';

const Home = () => {
    return (
        <div className="flex flex-col">
            <div className="w-full h-full">
                <div className="w-full h-full flex flex-col absolute space-y-96 py-4 items-center lg:space-y-0 lg:items-start lg:pt-32 lg:justify-start">
                    <input
                        className="outline-none p-3 rounded-full shadow-sm transition duration-300 focus-within:shadow-sm focus:ring-2 focus:w-10/12 lg:hidden"
                        placeholder="Ciudad" type="search" id="" name="" />
                        <div className="hidden h-auto lg:w-2/5 lg:flex pb-6">
                            <p className="text-4xl ml-16 font-bold text-black">Encuentra mas ubicaciones como esta.</p>
                        </div>
                    <button
                         className="w-48 rounded-full text-md text-primary font-semibold p-4 bg-white shadow-sm transition-all duration-500 ease-in-out hover:bg-primary hover:text-white transform hover:-traslate-y-1 hover:scale-110 lg:ml-16">
                        Explorar
                    </button>
                </div>
            </div>
            <div className="w-full h-full lg:h-96 lg:bg-sanFranciscoDesktop lg:bg-cover lg:bg-center">
                <img className="lg:hidden" src="https://raw.githubusercontent.com/platzi/PlatziTravel/main/public/img/sanFrancisco.jpg" />
            </div>
        </div>
    );
}

export default Home;