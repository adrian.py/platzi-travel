import React from "react";
import Card from "./Card";
import { CardType } from "@interfaces/card";

const Listado = () => {

    const recomendaciones: CardType[] = [
        { image: "norway", background_color: "secondary", text_color: "white", title: "Noruega", description: "Paisajes increibles" },
        { image: "new_york", background_color: "white", text_color: "terciary", title: "New York", description: "Paisajes increibles" },
        { image: "yosemite", background_color: "secondary", text_color: "white", title: "Ysemite", description: "Paisajes increibles" },
        { image: "seattle", background_color: "white", text_color: "terciary", title: "Seattle", description: "Paisajes increibles" },
        { image: "austin", background_color: "secondary", text_color: "terciary", title: "Austin, TX", description: "Paisajes increibles" },
        { image: "wayoming", background_color: "white", text_color: "terciary", title: "Wayoming", description: "Paisajes increibles" },
        { image: "dallas", background_color: "secondary", text_color: "terciary", title: "Dallas, TX", description: "Paisajes increibles" },
    ];

    return (
        <React.Fragment>
            {recomendaciones.map((c/* : CardType */) => (
                <Card key={c.title}
                    image={c.image} title={c.title}
                    description={c.description} background_color={c.background_color}
                    text_color={c.text_color} />
            ))}
        </React.Fragment>
    );
}
export default Listado;