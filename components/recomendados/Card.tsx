import { CardType } from '@interfaces/card';

const Card = ({image, title, description, background_color, text_color}: CardType) => {

    return (
        <div className="Card">
           <div className={`w-full h-3/5 rounded-t-lg bg-norway bg-cover`}></div>
           <div className={`w-full h-2/5 bg-white shadow-md rounded-b-md py-2 dark:bg-gray-700`}>
               <p className={`text-terciary text-bold text-xl px-4 dark:text-white`}>{title}</p>
               <p className={`text-terciary text-md px-4 dark:text-white`}>{description}</p>
           </div>
       </div>
    );

}
export default Card;