const Listado = () => {
    return (
        <div className="w-full h-full flex flex-col space-y-6 items-center justify-center px-4">
            <div className="CardTop bg-chicago px-8">
                <p className="CardTitle">Chicago</p>
                <p className="text-sm pl-8 text-white mr-15">2 habitaciones, 1 baño y cocina</p>
            </div>
            <div className="lg:flex lg:full hidden lg:w-full lg:space-x-4">
                <div className="CardTop bg-LA lg:h-auto">
                    <p className="CardTitle">Los Angeles</p>
                    <p className="text-sm pl-8 text-white mr-15"> 8 habitaciones, 3 baños, cocina y picina privada.</p>
                </div>
                <div className="w-full h-full">
                    <div className="CardTop bg-miami">
                        <p className="CardTitle">Miami</p>
                        <p className="text-sm pl-8 text-white mr-15"> 3 habitaciones, 2 baños, cocina y una increible vista.</p>
                    </div>
                    <div className="CardTop bg-bali">
                        <p className="CardTitle">Bali</p>
                        <p className="text-sm pl-8 text-white mr-15"> 2 habitaciones, 2 baños, cocina y pricina privada.</p>
                    </div>
                </div>

            </div>

        </div>
    );
}
export default Listado;