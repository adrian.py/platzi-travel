
const CardTop = () => {
    return (
        <div className="w-full h-96 bg-bali bg-cover rounded-xl ">
            <p className="CardTitle">Bali</p>
            <p className="text-sm pl-8 text-white mr-15"> 2 habitaciones, 2 baños, cocina y pricina privada.</p>
        </div>
    );
}
export default CardTop;