const Footer = () => {
    return (
        <footer className="w-full h-auto bg-gray-100 space-y-2" id="footer">
            <p className="text-lg">Acerca de nosotros</p>
            <p className="text-sm text-gray-300">Inversionistas</p>
            <p className="text-sm text-gray-300">Empleos</p>
            <p className="text-sm text-gray-300">Terminos y condiciones</p>
            <p className="text-sm text-gray-300">Contacto</p>
        </footer>
    );
}
export default Footer;